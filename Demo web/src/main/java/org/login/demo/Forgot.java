package org.login.demo;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

import java.io.IOException;

import javax.swing.*;

import org.login.resource.*;

@WebServlet("/forgot")
public class Forgot extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
  
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		JFrame f=new JFrame();
		LoginDao dao=new LoginDao();
		HttpSession session=request.getSession();
		String mail=(String) session.getAttribute("Email");
		String number=(String) session.getAttribute("Number");
		int otp=(int)session.getAttribute("OTP");
		System.out.println("The mail otp is: "+otp);
		String userOtp=(String)request.getParameter("OTP");
		int user=Integer.valueOf(userOtp);
		System.out.println("The given user otp is: "+user);
		String pass=(String)request.getParameter("Password");
		String confirm=(String)request.getParameter("Confirm");
		if(!pass.equals(confirm)) {
			JOptionPane.showMessageDialog(f, "Your password and confirm password is not same");
			response.sendRedirect("Update.jsp");
		}
		else {
			if(otp==user) {
					dao.update(mail,number, pass);
				
					JOptionPane.showMessageDialog(f, "Your password changed successfully");
					response.sendRedirect("index.jsp");
			}
			else {
				JOptionPane.showMessageDialog(f, "Your Otp is not correct");
				response.sendRedirect("Update.jsp");
			}
		}
	}

}
