package org.login.demo;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;

import javax.swing.*;

import org.login.resource.*;

@WebServlet("/signup")
public class Signup extends HttpServlet {
	private static final long serialVersionUID = 1L;
     
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		JFrame f=new JFrame();
		String name=(String) request.getParameter("Name");
		String email=(String) request.getParameter("Email");
		String mobile=(String) request.getParameter("Mobile");
		String pass=(String) request.getParameter("Password");
		int otp=(int)(Math.random()*900000)+100000;
		LoginDao dao=new LoginDao();
			if(dao.check(email, mobile)) {
				JOptionPane.showMessageDialog(f,"YOUR EMAIL OR MOBILE NUMBER ALREADY EXISTS");
				response.sendRedirect("signup.jsp");
			}
		
		String message="Your otp to register your detail is: "+otp;
		String apikey="KdMARu5tZ0fmSQUJzFcEoOTlx39yWDihGHaL4jN6kbn18we7IplnwKUR4vX6Os1BoL2zdQe579tCI8hj";
	    TextMessage.sendOTP(message, mobile, apikey);
	    System.out.println("Message sent successfully");
	    int userotp=Integer.valueOf(JOptionPane.showInputDialog("Enter your Mobile otp?"));
		if(otp==userotp) {			
		   
				if(dao.signup(name, email, mobile, pass)) {
					JOptionPane.showMessageDialog(f,"<html><b style=\"font-size:10px;\">Your data added Successfully, Please login</b></html>");
					response.sendRedirect("index.jsp");
				}
			
		}
	    
	    else {
	    	JOptionPane.showMessageDialog(f,"Error occured,Your Otp is not correct, Please try again");
	    	response.sendRedirect("signup.jsp");
	    }
	}

}
