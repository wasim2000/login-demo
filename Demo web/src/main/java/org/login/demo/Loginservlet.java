package org.login.demo;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

import java.io.IOException;
import javax.swing.*;

import org.login.resource.*;

/**
 
 */
@WebServlet("/login")
public class Loginservlet extends HttpServlet {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String uname=(String)request.getParameter("username");
		String pass=(String)request.getParameter("password");
		LoginDao dao=new LoginDao();
		HttpSession session=request.getSession();
		JFrame f=new JFrame();
		try {
			if(dao.service(uname, pass)) {
				String name=dao.getName();
				session.setAttribute("name",name);
				response.sendRedirect("welcome.jsp");
			}
			else {
				JOptionPane.showMessageDialog(f, "<html><b style=\"font-size:10px;\">Username or password is incorrect,please try again</b></html>");
				response.sendRedirect("index.jsp");
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
