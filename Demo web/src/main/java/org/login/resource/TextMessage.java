package org.login.resource;
import javax.net.ssl.HttpsURLConnection;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLEncoder;

public class TextMessage {
    public static boolean sendOTP(String message,String number,String apiKey) {
        try{
            String sendId="TXTIND";
            String language="english";

            message=URLEncoder.encode(message,"UTF-8");

            String myUrl="https://www.fast2sms.com/dev/bulkV2?authorization="+apiKey+"&sender_id="+sendId+"&message="+message+"&language="+language+"&route=v3&numbers="+number;
            URL url=new URL(myUrl);
            HttpsURLConnection con=(HttpsURLConnection) url.openConnection();
            con.setRequestMethod("GET");
            con.setRequestProperty("User-Agent","Mozilla/5.0");
            con.setRequestProperty("cache-control","no-cache");
            System.out.println("Wait---");
            int responseCode=con.getResponseCode();
            System.out.println("Response code: "+responseCode);

            BufferedReader  br=new BufferedReader(new InputStreamReader(con.getInputStream()));
            String inputLine;
            while ( (inputLine= br.readLine())!=null )  {
                System.out.println(inputLine);
            }
            br.close();
            return true;
        } catch (Exception e) {
            System.out.println(e);
            return false;
        }
    }
}
