package org.login.resource;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;



public class LoginDao {
	Connection con = null;
    PreparedStatement stmt = null;
    ResultSet rs = null;
    private String name;
	boolean flag=false; 
    public LoginDao(){
        try{
            Class.forName("com.mysql.cj.jdbc.Driver");
            con = DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/Accounts","root","root");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public boolean check(String mail,String number) {
    	try {
    	 PreparedStatement stmt=con.prepareStatement("select * from PersonDetails  where Email_id = ? or Mobile_Number=?");
	     stmt.setString(1,mail);
	     stmt.setString(2,number);
	     ResultSet rs=stmt.executeQuery();
	     while(rs.next()) {
	    	 name=rs.getString(2);
	    	 return true;
	     }
    	} catch (Exception e) {
    		e.printStackTrace();
    	}
    	
    	return false;
    }
    public boolean update(String mail,String number,String password){
    	try {
    	  PreparedStatement stmt=con.prepareStatement("update PersonDetails set Password=? where Email_id=? or Mobile_Number=?");
    	  stmt.setString(3, number);
    	  stmt.setString(2, mail);
    	  stmt.setString(1, password);
    	  int n=stmt.executeUpdate();
    	  if(n==0) {
    		
    		  return false;
    	  }
    	} catch(Exception e) {
    		e.printStackTrace();
    	}
    	
    	return true;
    }
	public boolean signup(String Name,String Email,String Mobile,String Pass)  {
		try {
			PreparedStatement stmt=con.prepareStatement("insert into PersonDetails(Name,Email_id,Mobile_Number,Password) values(?,?,?,?)");
		    stmt.setString(1,Name);
		    stmt.setString(2,Email);
		    stmt.setString(3, Mobile);
		    stmt.setString(4, Pass);
		    int n=stmt.executeUpdate();
		    if(n==0) {
		    	con.close();
		    	return false;
		    }
		}catch (Exception e) {
			e.printStackTrace();
		}
		return true;
	}
	public boolean service(String uname,String pass)  {
		try {
	     PreparedStatement stmt=con.prepareStatement("select * from PersonDetails  where Email_id = ? and Password=? ");
	     stmt.setString(1, uname);
	     stmt.setString(2, pass);
	     ResultSet rs=stmt.executeQuery();
	     while(rs.next()) {
	    	 name=rs.getString(2);
	    	 con.close();
	    	 flag=true;
	     }
		} catch(Exception se) {
			System.out.println(se);
		} 
	  
	    return flag;
	}
	
	
	public String getName() {
		return name;
	}
}
